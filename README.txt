HOW TO RUN GAME:

Normal mode: Run "Simplified GWENT Project.exe" in the "Build" folder
Headless mode:
	- If you can run .sh files, then run RunHeadlessMode.sh in the "Build" folder (It allows you to watch AI moves in realtime on the console)
	- Else run RunHeadlessMode.bat in the "Build" folder (It prints all the AI moves on the file "lastMatchLog.txt")

HOW A DESIGNER CAN CREATE AN ABILITY:

- Open the Unity Project
- Go into the "Abilities" folder
- Right click in the "Project Window" > Create > Ability
- Give a name to the Ability object
- Set the functioning of the ability from the Inspector (Just set the options without writing code)
- Assign the new Ability to a Card

OTHER IMPORTANT INFOS:

The Unity version of the project is "Unity 2018.4.7f1"
The list of the abilities and the networking UML diagram are in the main folder