﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SmartMVC;

public class ClientManager : MonoBehaviour
{
    public static ClientManager instance;
    public bool spectating = false;

    public Button btnPlay;
    public Button btnSpectate;
    public Button btnQuit;

    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        if (Application.isBatchMode)
        {
            StartGame(true);
            return;
        }
        btnPlay.onClick.AddListener(OnClickPlay);
        btnSpectate.onClick.AddListener(OnClickSpectate);
        btnQuit.onClick.AddListener(OnClickClose);
    }

    void OnClickPlay()
    {
        StartGame(false);
    }

    void OnClickSpectate()
    {
        StartGame(true);
    }

    void OnClickClose()
    {
        Application.Quit();
    }

    void StartGame(bool spectateAIGame)
    {
        GameController.Log("Starting game...");
        spectating = spectateAIGame;
        UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
        gameObject.SetActive(false);
    }
}
