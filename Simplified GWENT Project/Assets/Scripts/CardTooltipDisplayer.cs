﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardTooltipDisplayer : MonoBehaviour
{
    static CardTooltipDisplayer instance;

    [SerializeField]
    Text txtName;
    Text TxtName { get => txtName; set => txtName = value; }
    [SerializeField]
    Text txtDescription;
    Text TxtDescription { get => txtDescription; set => txtDescription = value; }

    Camera UICamera
    {
        get { return GameApplication.instance.model.HUDCamera; }
    }

    /// <summary>
    /// The Rect Transform component of the UI Canvas
    /// </summary>
    RectTransform canvasRectTransform
    {
        get { return GameApplication.instance.view.HUDView.HUDCanvas as RectTransform; }
    }

    /// <summary>
    /// The Rect Transform component of the Tooltip
    /// </summary>
    RectTransform rectTransform
    {
        get { return transform as RectTransform; }
    }


    void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Shows the tooltip using the card data
    /// </summary>
    /// <param name="name">The card name</param>
    /// <param name="description">The card description</param>
    void Show(string name, string description)
    {
        gameObject.SetActive(true);
        TxtName.text = name;
        TxtDescription.text = description;
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, Input.mousePosition, UICamera, out localPoint);
        float tooltipXPosition = localPoint.x + (rectTransform.sizeDelta.x / 2);
        float outScreenPixelsX = (localPoint.x + rectTransform.sizeDelta.x) - (Screen.width / 2);
        float tooltipYPosition = localPoint.y + (rectTransform.sizeDelta.y / 2);
        float outScreenPixelsY = (localPoint.y + rectTransform.sizeDelta.y) - (Screen.height / 2);
        if (outScreenPixelsX > 0)
        {
            tooltipXPosition -= outScreenPixelsX;
        }
        if (outScreenPixelsY > 0)
        {
            tooltipYPosition -= outScreenPixelsY;
        }

        transform.localPosition = new Vector2
        (
            tooltipXPosition,
            tooltipYPosition
        );
    }

    /// <summary>
    /// Hides the tooltip
    /// </summary>
    void Hide()
    {
        gameObject.SetActive(false);
    }

    public static void ShowTooltip(string name, string description)
    {
        instance.Show(name, description);
    }

    public static void HideTooltip()
    {
        instance.Hide();
    }
}
