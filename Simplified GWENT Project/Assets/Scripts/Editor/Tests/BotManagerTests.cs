﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

namespace Tests
{
    public class BotManagerTests
    {
        [Test]
        public void DecideCardPosition_Test()
        {
            CardManager cardManager = new GameObject().AddComponent<CardManager>();
            cardManager.card = new Card
            {
                ability = new Ability
                {
                    requiredPosition = Ability.AbilityRequiredPosition.melee
                }
            };
            Card.CardPosition expectedCardPosition = Card.CardPosition.melee;

            BotManager botManager = new GameObject().AddComponent<BotManager>();
            Card.CardPosition decidedPosition = botManager.DecideCardPosition(cardManager);

            Assert.AreEqual(expectedCardPosition, decidedPosition);
        }
    }
}
