﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CardManagerTests
    {
        CardManager[] cardManagers;

        [SetUp]
        public void Setup()
        {
            cardManagers = new CardManager[2];
            for (int i = 0; i < cardManagers.Length; i++)
            {
                cardManagers[i] = new GameObject().AddComponent<CardManager>();
                cardManagers[i].card = Resources.Load<Card>("Tests/TestCard" + (i + 1));
                cardManagers[i].owner = new GameObject().AddComponent<PlayerManager>();
                cardManagers[i].owner.cardsOnField = new List<CardManager>();
                cardManagers[i].currentCardValue = cardManagers[i].card.cardValue;
            }
        }

        [Test]
        public void BuffCard_Test()
        {
            int expectedBuffedValue = cardManagers[1].currentCardValue + cardManagers[0].card.ability.effectAmount;

            cardManagers[0].card.ability.ApplyEffectOnTarget
            (
                cardManagers[0].owner,
                cardManagers[1],
                cardManagers[0],
                1,
                true
            );

            Assert.AreEqual(expectedBuffedValue, cardManagers[1].currentCardValue);
        }

        [Test]
        public void DebuffCard_Test()
        {
            int expectedDebuffedValue = cardManagers[0].currentCardValue - cardManagers[1].card.ability.effectAmount;

            cardManagers[1].card.ability.ApplyEffectOnTarget
            (
                cardManagers[1].owner,
                cardManagers[0],
                cardManagers[1],
                1,
                true
            );

            Assert.AreEqual(expectedDebuffedValue, cardManagers[0].currentCardValue);
        }
    }
}
