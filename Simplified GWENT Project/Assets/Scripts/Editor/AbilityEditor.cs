﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Ability))]
public class AbilityEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Ability ability = (Ability)target;
        if (ability.type == Ability.AbilityType.passive)
        {
            ability.castTrigger = (Ability.AbilityCastTrigger)EditorGUILayout.EnumPopup
            (
                new GUIContent("Cast Trigger", "The event that triggers the ability cast"), 
                ability.castTrigger
            );
        }
    }
}
