﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ability", menuName = "Ability")]
public class Ability : ScriptableObject
{
    #region Enums

    public enum AbilityType
    {
        passive,
        active
    }

    public enum AbilityEffect
    {
        buff,
        debuff
    }

    public enum AbilityCastMode
    {
        random,
        self
    }

    public enum AbilityCastTrigger
    {
        deploy,
        receiveDamage,
        allyReceiveDamage,
        death,
        allyDeath
    }

    public enum AbilityRequiredPosition
    {        
        melee,
        ranged,
        any
    }

    #endregion
    
    [Tooltip("The type of the ability, it can be Active or Passive")]
    public AbilityType type;
    [HideInInspector]
    public AbilityCastTrigger castTrigger;
    [Tooltip("The effect of the ability, it can Buff or Debuff other cards")]
    public AbilityEffect effect;
    [Tooltip("How the ability is casted, it can be casted by player selection or randomly")]
    public AbilityCastMode castMode;
    [Tooltip("The field position needed to cast the ability")]
    public AbilityRequiredPosition requiredPosition;
    [Tooltip("How many cards are affected by this ability")]
    [Range(0, 10)]
    public byte cardsToAffect = 0;
    [Tooltip("The amount of points to buff/debuff")]
    [Range(0, 127)]
    public byte effectAmount = 0;
    public string abilityDescription;
    public ParticleSystemManager abilityParticles;

    /// <summary>
    /// Casts this ability
    /// </summary>
    /// <param name="caster">Source card owner</param>
    /// <param name="source">Source card</param>
    /// <param name="cardPosition">Current source card position</param>
    public void CastAbility(PlayerManager caster, CardManager source, Card.CardPosition cardPosition)
    {
        if (requiredPosition != AbilityRequiredPosition.any
            && requiredPosition != (AbilityRequiredPosition)cardPosition)
        {
            source.CallOnCardCastedAbility();
            return;
        }

        switch (type)
        {
            case AbilityType.passive:
                CastPassiveAbility(caster, source);
                break;
            case AbilityType.active:
                if (GetAmountOfCardsToTarget(caster) < 1)
                {
                    source.CallOnCardCastedAbility();
                    return;
                }
                if (caster.isLocalPlayer)
                {
                    Cursor.SetCursor(GameApplication.instance.model.targetCursor, Vector2.zero, CursorMode.Auto);
                }
                source.abilityTargetsNeeded = GetAmountOfCardsToTarget(caster);
                break;
        }
    }

    /// <summary>
    /// Cast a passive ability, according to the cast mode
    /// </summary>
    /// <param name="caster"></param>
    /// <param name="source"></param>
    void CastPassiveAbility(PlayerManager caster, CardManager source)
    {
        switch (castMode)
        {
            case AbilityCastMode.random:
                ApplyEffectOnRandomTargets(caster, source);
                break;
            case AbilityCastMode.self:
                ApplyEffectOnTarget(caster, source, source, 0);
                break;
        }
    }

    /// <summary>
    /// Applies ability effect on random targets
    /// </summary>
    /// <param name="caster">Sourca card owner</param>
    /// <param name="source">Source card</param>
    void ApplyEffectOnRandomTargets(PlayerManager caster, CardManager source)
    {
        source.abilityTargetsNeeded = GetAmountOfCardsToTarget(caster);
        if (source.abilityTargetsNeeded < 1)
        {
            source.CallOnCardCastedAbility();
            return;
        }
        for (int i = 0; i < source.abilityTargetsNeeded; i++)
        {
            CardManager target;
            do
            {
                target = GetRandomCard(caster);
            }
            while (source.abilityTargets.Contains(target));

            source.abilityTargets.Add(target);
            ApplyEffectOnTarget(caster, target, source, i + 1);
        }
    }

    /// <summary>
    /// According to the ability's info and the card on field, gets the amount of targettable cards 
    /// </summary>
    /// <param name="caster">Source card owner</param>
    /// <returns>Amount of targettable cards</returns>
    int GetAmountOfCardsToTarget(PlayerManager caster)
    {
        PlayerManager targetPlayer = caster;

        if (effect == AbilityEffect.debuff)
        {
            targetPlayer = GameApplication.instance.model.GetPlayerOpponent(caster);
        }
        //The cards that buff allies, can buff themselves
        else if ((effect == AbilityEffect.buff && type == AbilityType.active)
            && targetPlayer.cardsOnField.Count < 1)
        {
            return 1;
        }

        if (targetPlayer.cardsOnField.Count < cardsToAffect)
        {
            return targetPlayer.cardsOnField.Count;
        }
        return cardsToAffect;
    }

    /// <summary>
    /// According to the ability effect return a random target card
    /// </summary>
    /// <param name="caster">Source card owner</param>
    /// <returns>A target card</returns>
    CardManager GetRandomCard(PlayerManager caster)
    {
        foreach (PlayerManager player in GameApplication.instance.model.players)
        {
            if ((player == caster) && (effect == AbilityEffect.buff) ||
                (player != caster) && (effect == AbilityEffect.debuff))
            {
                return player.cardsOnField[Random.Range(0, player.cardsOnField.Count)];
            }
        }
        return null;
    }

    /// <summary>
    /// Applies ability effects on the target card
    /// </summary>
    /// <param name="caster">Source card owner</param>
    /// <param name="target">Target card</param>
    /// <param name="source">Source card</param>
    /// <param name="affectedCardIndex">The ID that identifies the target card in the list of ability's targets</param>
    public void ApplyEffectOnTarget(PlayerManager caster, CardManager target, CardManager source, int affectedCardIndex, bool testing = false)
    {
        target.ApplyAbilityEffect(caster, effect, effectAmount);
        if (testing) { return; }

        GameController.Log(source.card.cardName + " casts its ability: [" + GetAbilityDescription() +
            "]\nThe ability target is: " + target.card.cardName, source.gameObject);
        if (Application.isBatchMode)
        {
            if (affectedCardIndex == source.abilityTargetsNeeded)
            {
                source.CallOnCardCastedAbility();
            }
            return;
        }
        if (!abilityParticles && affectedCardIndex == source.abilityTargetsNeeded)
        {
            source.CallOnCardCastedAbility();
            target.ApplyAbilityEffectOnUI();
            return;
        }
        ParticleSystemManager particleManager = Instantiate(abilityParticles, target.transform);
        particleManager.transform.localPosition = new Vector3(0, 0, -20);
        particleManager.SetCallback(OnParticlesStopped, target, source, affectedCardIndex);
    }

    /// <summary>
    /// Called when the ability particles stops
    /// </summary>
    /// <param name="target">Target card</param>
    /// <param name="source">Source card</param>
    void OnParticlesStopped(CardManager target, CardManager source, int affectedCardIndex)
    {
        if (affectedCardIndex == source.abilityTargetsNeeded)
        {
            source.CallOnCardCastedAbility();
        }
        target.ApplyAbilityEffectOnUI();
    }

    /// <summary>
    /// Format the ability description
    /// </summary>
    /// <returns>Formatted description</returns>
    public string GetAbilityDescription()
    {
        string description = "{castMoment} ({requiredPosition}): " + abilityDescription;
        description = description.Replace("{castMoment}", castTrigger.ToString().FirstLetterToUppercase().FormatByCapitalChars());
        description = description.Replace("{requiredPosition}", requiredPosition.ToString().FirstLetterToUppercase());
        description = description.Replace("{cardsToAffect}", cardsToAffect.ToString().FirstLetterToUppercase());
        description = description.Replace("{effectAmount}", effectAmount.ToString().FirstLetterToUppercase());
        return description;        
    }

    void OnValidate()
    {
        if (type == Ability.AbilityType.active)
        {
            castTrigger = Ability.AbilityCastTrigger.deploy;
        }
    }
}
