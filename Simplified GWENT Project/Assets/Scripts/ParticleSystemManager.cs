﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemManager : MonoBehaviour
{
    System.Action<CardManager, CardManager, int> callback;
    CardManager affectedCard;
    CardManager sourceCard;
    int affectedCardIndex;
    
    /// <summary>
    /// Sets the callback to call when the particle system stops
    /// </summary>
    /// <param name="action">The callback to call</param>
    /// <param name="card">The target card</param>
    /// <param name="source">The source card</param>
    /// <param name="cardID">The ID that identifies the target card in the list of ability's targets</param>
    public void SetCallback(System.Action<CardManager, CardManager, int> action, CardManager card, CardManager source, int cardID)
    {
        callback = action;
        affectedCard = card;
        sourceCard = source;
        affectedCardIndex = cardID;
    }

    void OnParticleSystemStopped()
    {
        if (callback != null)
        {
            callback(affectedCard, sourceCard, affectedCardIndex);
            callback = null;
        }
        Destroy(gameObject, 1);
    }
}
