﻿using System;
using System.Text.RegularExpressions;
using System.Collections;

public static class StringExtensions
{
    /// <summary>
    /// Capitalizes the first letter of the string
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string FirstLetterToUppercase(this string text)
    {
        switch (text)
        {
            case null: throw new ArgumentNullException(nameof(text));
            case "": throw new ArgumentException($"{nameof(text)} cannot be empty", nameof(text));
            default: return text[0].ToString().ToUpper() + text.Substring(1);
        }        
    }

    /// <summary>
    /// Insert a space between the words of the string (Only CamelCase format)
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string FormatByCapitalChars(this string text)
    {
        switch (text)
        {
            case null: throw new ArgumentNullException(nameof(text));
            case "": throw new ArgumentException($"{nameof(text)} cannot be empty", nameof(text));
            default:
                string[] words = Regex.Split(text, @"(?<!^)(?=[A-Z])");
                text = words[0];
                for (int i = 1; i < words.Length; i++)
                {
                    text += " " + words[i];
                }
                return text;
        }
    }
}
