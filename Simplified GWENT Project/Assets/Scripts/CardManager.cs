﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardManager : MonoBehaviour
{
    #region Delegates and Events

    public delegate bool OnCardPlacedHandler(CardManager card);
    public static event OnCardPlacedHandler OnTryToPlaceCard;
    public delegate void CardEventsHandler(CardManager card);
    public static event CardEventsHandler OnCardSelected;
    public static event CardEventsHandler OnCardReceivedDamage;
    public static event CardEventsHandler OnCardDied;

    #endregion
    #region References

    /// <summary>
    /// The Rect Transform component of the UI Canvas
    /// </summary>
    RectTransform canvasRectTransform
    {
        get { return GameApplication.instance.view.HUDView.HUDCanvas as RectTransform; }
    }

    Camera UICamera
    {
        get { return GameApplication.instance.model.HUDCamera; }
    }

    #endregion
    #region Graphics
    
    [SerializeField]
    Image imgFlippedCard;
    Image ImgFlippedCard { get => imgFlippedCard; set => imgFlippedCard = value; }

    [SerializeField]
    Image imgFaction;
    Image ImgFaction { get => imgFaction; set => imgFaction = value; }

    [SerializeField]
    Image imgCardImage;
    Image ImgCardImage { get => imgCardImage; set => imgCardImage = value; }

    [SerializeField]
    Image imgCardBorder;
    Image ImgCardBorder { get => imgCardBorder; set => imgCardBorder = value; }

    public Image imgCastingAbility;

    [SerializeField]
    Text txtCardValue;
    Text TxtCardValue { get => txtCardValue; set => txtCardValue = value; }

    Button button;

    #endregion

    /// <summary>
    /// Is this card dead?
    /// </summary>
    public bool IsDead
    {
        get { return currentCardValue < 1; }
    }

    /// <summary>
    /// The represented card
    /// </summary>
    public Card card;
    /// <summary>
    /// The current position of this card on the field
    /// </summary>
    public Card.CardPosition currentCardPosition;
    /// <summary>
    /// The player who is owner of this card
    /// </summary>
    public PlayerManager owner;

    bool isGrabbedByPlayer = false;
    public bool isPlacedOnField = false;
    public bool wasReplacedInHand = false;

    /// <summary>
    /// How many targets needs the card's ability
    /// </summary>
    public int abilityTargetsNeeded;
    /// <summary>
    /// The targets of the card's ability
    /// </summary>
    public List<CardManager> abilityTargets;
    /// <summary>
    /// The points this card has
    /// </summary>
    public int currentCardValue;

    void Start()
    {
        abilityTargets = new List<CardManager>();
        SetCardGlowAlpha(0, 0);
    }

    void OnDestroy()
    {
        if (card.ability.castTrigger == Ability.AbilityCastTrigger.allyReceiveDamage ||
            card.ability.castTrigger == Ability.AbilityCastTrigger.receiveDamage)
        {
            OnCardReceivedDamage -= TriggerCardDamageCast;
        }
        if (card.ability.castTrigger == Ability.AbilityCastTrigger.allyDeath ||
            card.ability.castTrigger == Ability.AbilityCastTrigger.death)
        {
            OnCardDied -= TriggerCardDeathCast;
        }
        if (PlayerManager.localPlayer == null) { return; }
        if (owner != PlayerManager.localPlayer) { return; }
        if (card.ability.type == Ability.AbilityType.active)
        {
            OnCardSelected -= TargetCard;
        }
    }

    /// <summary>
    /// Initializes the card
    /// </summary>
    /// <param name="cardOwner">The player who owns this card</param>
    /// <param name="representedCard">Which card is represented</param>
    /// <param name="turnCard">Does this card have to be turned?</param>
    public void Initialize(PlayerManager cardOwner, Card representedCard, bool turnCard)
    {
        card = representedCard;
        owner = cardOwner;
        gameObject.name = card.name;
        currentCardValue = card.cardValue;
        TxtCardValue.text = currentCardValue.ToString();
        ImgCardImage.sprite = card.sprCardImage;
        button = GetComponent<Button>();
        button.onClick.AddListener(OnButtonClick);
        TurnCard(!turnCard);
        if (ClientManager.instance.spectating)
        {
            ClearDragEvents();
        }
    }

    public void SetDelegates()
    {
        if (card.ability.castTrigger == Ability.AbilityCastTrigger.allyDeath ||
            card.ability.castTrigger == Ability.AbilityCastTrigger.death)
        {
            OnCardDied += TriggerCardDeathCast;
        }
        if (card.ability.castTrigger == Ability.AbilityCastTrigger.allyReceiveDamage ||
            card.ability.castTrigger == Ability.AbilityCastTrigger.receiveDamage)
        {
            OnCardReceivedDamage += TriggerCardDamageCast;
        }
        if (PlayerManager.localPlayer == null) { return; }
        if (owner != PlayerManager.localPlayer) { return; }
        if (card.ability.type == Ability.AbilityType.active)
        {
            OnCardSelected += TargetCard;
        }
    }

    /// <summary>
    /// This function is called when the mouse pointer enters the card
    /// </summary>
    public void OnPointerEnterCard()
    {
        CardTooltipDisplayer.ShowTooltip(card.cardName, card.ability.GetAbilityDescription());
        if (isPlacedOnField || PlayerManager.localPlayer == null) { return; }
        transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + 120);
    }

    /// <summary>
    /// This function is called when the mouse pointer exits the card
    /// </summary>
    public void OnPointerExitCard()
    {
        if (wasReplacedInHand)
        {
            wasReplacedInHand = false;
            return;
        }
        CardTooltipDisplayer.HideTooltip();
        if (isPlacedOnField || PlayerManager.localPlayer == null) { return; }
        transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - 120);
    }

    /// <summary>
    /// This function is called when the mouse begin to drag the card
    /// </summary>
    public void OnBeginDragCard()
    {
        GameApplication.instance.NotifyEvent(SmartMVC.Events.GLOW_FIELD_CONTAINERS, 1f, true);
        CardTooltipDisplayer.HideTooltip();
        transform.SetParent(GameApplication.instance.view.HUDView.HUDCanvas, true);
        isGrabbedByPlayer = true;
        GameApplication.instance.model.currentGrabbedCard = this;
    }

    /// <summary>
    /// This function is called when the mouse begin to drag the card
    /// </summary>
    public void OnEndDragCard()
    {
        GameApplication.instance.NotifyEvent(SmartMVC.Events.GLOW_FIELD_CONTAINERS, 0f, true);
        if (OnTryToPlaceCard == null) { return; }
        isPlacedOnField = OnTryToPlaceCard(this);
        isGrabbedByPlayer = false;
    }

    /// <summary>
    /// Removes all the drag events from the Event Trigger
    /// </summary>
    public void ClearDragEvents()
    {
        EventTrigger eventTrigger = GetComponent<EventTrigger>();
        for (int i = eventTrigger.triggers.Count - 1; i >= 0; i--)
        {
            if (eventTrigger.triggers[i].eventID == EventTriggerType.BeginDrag ||
                eventTrigger.triggers[i].eventID == EventTriggerType.EndDrag)
            {
                eventTrigger.triggers.RemoveAt(i);
            }
        }
    }

    void OnButtonClick()
    {
        if (!isPlacedOnField) { return; }
        if (OnCardSelected != null)
        {
            OnCardSelected(this);
        }
    }

    /// <summary>
    /// Targets the selected card
    /// </summary>
    /// <param name="cardManager"></param>
    public void TargetCard(CardManager cardManager)
    {
        if (abilityTargetsNeeded < 1) { return; }
        if ((cardManager.owner != owner && card.ability.effect == Ability.AbilityEffect.buff) ||
            (cardManager.owner == owner && card.ability.effect == Ability.AbilityEffect.debuff)) { return; }
        abilityTargets.Add(cardManager);
        cardManager.SetCardGlowAlpha(1, 0.5f);
        if (abilityTargets.Count < abilityTargetsNeeded) { return; }
        if (owner.isLocalPlayer)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
        for (int i = 0; i < abilityTargets.Count; i++)
        {
            abilityTargets[i].SetCardGlowAlpha(0, 0);
            card.ability.ApplyEffectOnTarget(owner, abilityTargets[i], this, i + 1);
        }
    }

    /// <summary>
    /// Called when a card dies
    /// </summary>
    /// <param name="cardManager">The dead card</param>
    void TriggerCardDeathCast(CardManager cardManager)
    {
        if (IsDead)
        {
            OnCardDied -= TriggerCardDeathCast;
            return;
        }
        if ((cardManager == this && card.ability.castTrigger == Ability.AbilityCastTrigger.death) ||
            (cardManager != this && owner == cardManager.owner && card.ability.castTrigger == Ability.AbilityCastTrigger.allyDeath))
        {
            GameApplication.instance.model.cardsReadyToCastAbility.Add(this);
        }
    }

    /// <summary>
    /// Called when a card receive damage
    /// </summary>
    /// <param name="cardManager">The damaged card</param>
    void TriggerCardDamageCast(CardManager cardManager)
    {
        if (card.ability.castMode == Ability.AbilityCastMode.self && IsDead)
        {
            OnCardReceivedDamage -= TriggerCardDamageCast;
            return;
        }
        if ((cardManager == this && card.ability.castTrigger == Ability.AbilityCastTrigger.receiveDamage) ||
            (cardManager != this && owner == cardManager.owner && card.ability.castTrigger == Ability.AbilityCastTrigger.allyReceiveDamage))
        {
            GameApplication.instance.model.cardsReadyToCastAbility.Add(this);
        }
    }

    /// <summary>
    /// Apply the effects of the ability that targeted this card
    /// </summary>
    /// <param name="cardOwner"></param>
    /// <param name="abilityEffect"></param>
    /// <param name="effectAmount"></param>
    public void ApplyAbilityEffect(PlayerManager cardOwner, Ability.AbilityEffect abilityEffect, int effectAmount)
    {
        switch (abilityEffect)
        {
            case Ability.AbilityEffect.buff:
                currentCardValue += effectAmount;
                owner.UpdatePlayerScore(effectAmount, true);
                break;
            case Ability.AbilityEffect.debuff:
                int debuffedValue = currentCardValue - effectAmount;
                if (debuffedValue < 1)
                {
                    effectAmount = currentCardValue;
                    owner.cardsOnField.Remove(this);
                    if (OnCardDied != null)
                    {
                        OnCardDied(this);
                    }
                }
                currentCardValue -= effectAmount;
                owner.UpdatePlayerScore(effectAmount, false);
                if (OnCardReceivedDamage != null)
                {
                    OnCardReceivedDamage(this);
                }
                break;
        }
    }

    /// <summary>
    /// Refreshes UI after the ability cast
    /// </summary>
    public void ApplyAbilityEffectOnUI()
    {
        TxtCardValue.text = currentCardValue.ToString();
        owner.RefreshScoreUI();
        if (IsDead)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Turns the card and shows its back or its front
    /// </summary>
    /// <param name="showCard"></param>
    public void TurnCard(bool showCard)
    {
        if (Application.isBatchMode) { return; }
        ImgFlippedCard.gameObject.SetActive(!showCard);
        ImgFaction.gameObject.SetActive(showCard);
        ImgCardBorder.gameObject.SetActive(showCard);
        TxtCardValue.gameObject.SetActive(showCard);
        ImgCardImage.gameObject.SetActive(showCard);
    }

    /// <summary>
    /// Shows or hides the card glow effect
    /// </summary>
    /// <param name="alpha">Target alpha</param>
    /// <param name="duration">The duration of the fade</param>
    public void SetCardGlowAlpha(float alpha, float duration)
    {
        if (Application.isBatchMode) { return; }
        imgCastingAbility.CrossFadeAlpha(alpha, duration, false);
    }

    void Update()
    {
        if (!isGrabbedByPlayer) { return; }
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, Input.mousePosition, UICamera, out localPoint);
        transform.position = localPoint;
    }

    public void CallOnCardCastedAbility()
    {
        GameApplication.instance.NotifyEvent(SmartMVC.Events.CARD_CASTED_ABILITY, this);
    }
}