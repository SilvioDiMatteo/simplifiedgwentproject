﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SmartMVC;

public class PlayerManager : MonoBehaviour
{
    #region References

    protected GameApplication app
    {
        get { return GameApplication.instance; }
    }

    #endregion

    /// <summary>
    /// Is this the local player?
    /// </summary>
    public bool isLocalPlayer
    {
        get
        {
            if (PlayerManager.localPlayer == null) { return false; }
            return this == localPlayer;
        }
    }

    /// <summary>
    /// A reference to the local player (Replaceable in a Multiplayer Environment)
    /// </summary>
    public static PlayerManager localPlayer;

    /// <summary>
    /// The player's camera
    /// </summary>
    [SerializeField]
    protected Camera myCamera;
    /// <summary>
    /// The layer mask of the UI
    /// </summary>
    LayerMask UILayerMask;

    /// <summary>
    /// How many cards a player can have in its hand
    /// </summary>
    protected const int MAX_CARDS_IN_HAND = 6;
    /// <summary>
    /// The current player's score
    /// </summary>
    public int playerScore = 0;

    /// <summary>
    /// All the cards placed on the field by the player
    /// </summary>
    public List<CardManager> cardsOnField;
    /// <summary>
    /// All the cards in the player's hand
    /// </summary>
    public List<CardManager> cardsInHand;

    /// <summary>
    /// Has the player already placed a card in this turn?
    /// </summary>
    public bool alreadyPlacedCardInTurn = false;
    /// <summary>
    /// Is this player the one who is the nearest to the game camera?
    /// </summary>
    public bool isNearToCamera = false;
    /// <summary>
    /// Is this the turn of this player?
    /// </summary>
    bool isMyTurn = false;

    void Awake()
    {
        GameController.OnTurnStarted += OnTurnStart;
    }

    protected virtual void Start()
    {
        cardsOnField = new List<CardManager>();
        cardsInHand = new List<CardManager>();
        CardManager.OnTryToPlaceCard += TryToPlaceCard;
        PopulatePlayerHand();

        app.view.fieldView.GetComponent<Canvas>().worldCamera = myCamera;
        UILayerMask = 1 << LayerMask.NameToLayer("UI");
    }

    void OnDestroy()
    {
        CardManager.OnTryToPlaceCard -= TryToPlaceCard;
        GameController.OnTurnStarted -= OnTurnStart;
    }

    /// <summary>
    /// Instantiates all the cards in the player's hand
    /// </summary>
    protected void PopulatePlayerHand()
    {
        Transform cardsContainer = isNearToCamera ? app.view.HUDView.playerHand : app.view.fieldView.enemyHandContainer;
        for (int i = 0; i < MAX_CARDS_IN_HAND; i++)
        {
            CardManager card = Instantiate(app.model.cardPrefab, cardsContainer).GetComponent<CardManager>();
            card.Initialize(this, app.model.availableCards[Random.Range(0, app.model.availableCards.Length)],
                !isNearToCamera && !ClientManager.instance.spectating);
            cardsInHand.Add(card);
        }
    }

    /// <summary>
    /// Called when a card has been dragged out of the player's hand
    /// </summary>
    /// <param name="card">The dragged card</param>
    /// <returns>If the card is succesfully placed, returns true</returns>
    bool TryToPlaceCard(CardManager card)
    {
        if (!isMyTurn || alreadyPlacedCardInTurn)
        {
            app.NotifyEvent(Events.PLACE_CARD_IN_HAND, app.model.currentGrabbedCard);
            return false;
        }
        Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray, out RaycastHit hitInfo, 5000, UILayerMask))
        {
            app.NotifyEvent(Events.PLACE_CARD_IN_HAND, app.model.currentGrabbedCard);
            return false;
        }
        Card.CardPosition cardPosition;
        
        if (hitInfo.collider.CompareTag("PlayerMeleeCardsContainer"))
        {
            cardPosition = Card.CardPosition.melee;
        }
        else if (hitInfo.collider.CompareTag("PlayerRangedCardsContainer"))
        {
            cardPosition = Card.CardPosition.ranged;
        }
        else
        {
            app.NotifyEvent(Events.PLACE_CARD_IN_HAND, app.model.currentGrabbedCard);
            return false;
        }
        PlaceCard(app.model.currentGrabbedCard, hitInfo.collider.transform as RectTransform, cardPosition);
        return true;
    }

    /// <summary>
    /// Places a card on the field
    /// </summary>
    /// <param name="cardsContainer">The place where the card will be placed</param>
    /// <param name="cardPosition">The field position in which is placed the card</param>
    protected void PlaceCard(CardManager cardManager, RectTransform cardsContainer, Card.CardPosition cardPosition)
    {
        GameController.Log("Player " + (app.model.GetPlayerIndex(this) + 1) + " places: " + cardManager.card.cardName);
        cardManager.currentCardPosition = cardPosition;
        cardsInHand.Remove(cardManager);
        app.NotifyEvent(Events.PLACE_CARD_ON_FIELD, cardManager, cardsContainer);
        playerScore += cardManager.currentCardValue;
        RefreshScoreUI();

        if (cardManager.card.ability.type == Ability.AbilityType.active || 
            cardManager.card.ability.castTrigger == Ability.AbilityCastTrigger.deploy)
        {
            app.model.cardsReadyToCastAbility.Add(cardManager);
            cardManager.SetCardGlowAlpha(1, 0.5f);
            cardManager.card.ability.CastAbility(this, cardManager, cardPosition);
        }
        alreadyPlacedCardInTurn = true;
        cardsOnField.Add(cardManager);
    }

    /// <summary>
    /// Called when a new turn begins
    /// </summary>
    /// <param name="player"></param>
    protected virtual void OnTurnStart(PlayerManager player)
    {
        if (player != this) { return; }
        GameController.Log("It's Player " + (app.model.GetPlayerIndex(this) + 1) + " turn");
        isMyTurn = true;
        StartCoroutine(WaitUntilTurnEnd());
    }

    /// <summary>
    /// Waits until every ready card resolved its ability and then ends turn
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitUntilTurnEnd()
    {
        yield return new WaitUntil(() => alreadyPlacedCardInTurn);
        yield return new WaitUntil(() => app.model.cardsReadyToCastAbility.Count == 0);
        //Waits a second not to end the turn too quickly
        yield return new WaitForSeconds(1);
        app.NotifyEvent(Events.CHECK_END_GAME_CONDITION);
        app.NotifyEvent(Events.END_TURN, this);
        isMyTurn = false;
        alreadyPlacedCardInTurn = false;
    }   

    /// <summary>
    /// Updates the score of this player
    /// </summary>
    /// <param name="amount">The amount of points to sum/subtract</param>
    /// <param name="sum">Are these points to sum?</param>
    public void UpdatePlayerScore(int amount, bool sum)
    {
        if (sum)
        {
            playerScore += amount;
            return;
        }
        playerScore -= amount;
    }

    /// <summary>
    /// Notify to refresh the player score on the UI
    /// </summary>
    public void RefreshScoreUI()
    {
        app.NotifyEvent(Events.UPDATE_PLAYER_SCORE, playerScore, !isNearToCamera);
    }
}
