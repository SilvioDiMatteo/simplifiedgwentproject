﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ScriptableObject
{
    public enum CardPosition
    {
        melee,
        ranged
    }

    [Tooltip("The name of this card (It is shown in game)")]
    public string cardName;
    [Tooltip("The basic value of this card")]
    public int cardValue;
    [Tooltip("The sprite associated to this card")]
    public Sprite sprCardImage;
    [Tooltip("The ability that can be casted by this card")]
    public Ability ability;
}
