﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class BotManager : PlayerManager
{
    protected override void Start()
    {
        if (isNearToCamera)
        {
            myCamera.gameObject.SetActive(true);
            app.view.fieldView.GetComponent<Canvas>().worldCamera = myCamera;
        }
        cardsOnField = new List<CardManager>();
        cardsInHand = new List<CardManager>();
        PopulatePlayerHand();
    }

    protected override void OnTurnStart(PlayerManager player)
    {
        base.OnTurnStart(player);
        if (player != this) { return; }
        CardManager cardToPlace;
        if (cardsInHand.Count > 0)
        {
            cardToPlace = cardsInHand[Random.Range(0, cardsInHand.Count)];
            Card.CardPosition cardPosition = DecideCardPosition(cardToPlace);
            RectTransform container = GetCardsContainer(cardPosition);
            PlaceCard(cardToPlace, container, cardPosition);
            cardToPlace.isPlacedOnField = true;
            if (cardToPlace.card.ability.type == Ability.AbilityType.active)
            {
                StartCoroutine(CastActiveAbility(cardToPlace));
            }
        }
    }

    /// <summary>
    /// According to the position required by the card's ability, chooses the position
    /// </summary>
    /// <param name="cardToPlace">The card to place</param>
    /// <returns>The position where the card will be placed</returns>
    public Card.CardPosition DecideCardPosition(CardManager cardToPlace)
    {
        switch (cardToPlace.card.ability.requiredPosition)
        {
            case Ability.AbilityRequiredPosition.melee:
                return Card.CardPosition.melee;
            case Ability.AbilityRequiredPosition.ranged:
                return Card.CardPosition.ranged;
            case Ability.AbilityRequiredPosition.any:
            default:
                return (Card.CardPosition)Random.Range(0, 2);
        }
    }

    /// <summary>
    /// According to the card position chooses the right cards container
    /// </summary>
    /// <param name="cardPosition">The position where the card will be placed</param>
    /// <returns>The cards container on the field</returns>
    RectTransform GetCardsContainer(Card.CardPosition cardPosition)
    {
        if (cardPosition == Card.CardPosition.melee)
        {
            return isNearToCamera ? app.view.fieldView.localPlayerMeleeCardsContainer : app.view.fieldView.enemyMeleeCardsContainer;
        }
        return isNearToCamera ? app.view.fieldView.localPlayerRangedCardsContainer : app.view.fieldView.enemyRangedCardsContainer;
    }

    /// <summary>
    /// Allows AI to cast active abilities (On random targets)
    /// </summary>
    /// <param name="placedCard">The card that has the ability to cast</param>
    /// <returns></returns>
    IEnumerator CastActiveAbility(CardManager placedCard)
    {
        yield return new WaitForSeconds(0.5f);
        PlayerManager targetPlayer = this;
        if (placedCard.card.ability.effect == Ability.AbilityEffect.debuff)
        {
            targetPlayer = GameApplication.instance.model.GetPlayerOpponent(this);
        }

        if (targetPlayer.cardsOnField.Count < 1)
        {
            placedCard.CallOnCardCastedAbility();
            yield break;
        }
        List<CardManager> tempTargets = new List<CardManager>();
        for (int i = 0; i < placedCard.abilityTargetsNeeded; i++)
        {
            CardManager target;
            do
            {
                target = targetPlayer.cardsOnField[Random.Range(0, targetPlayer.cardsOnField.Count)];
            }
            while (tempTargets.Contains(target));
            tempTargets.Add(target);
            placedCard.TargetCard(target);
        }
    }
}
