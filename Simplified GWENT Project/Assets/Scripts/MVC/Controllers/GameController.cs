﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SmartMVC;

public class GameController : Controller<GameApplication>
{
    #region References

    GameModel gameModel
    {
        get { return app.model; }
    }

    GameView gameView
    {
        get { return app.view; }
    }

    #endregion

    public delegate void OnTurnStartedHandler(PlayerManager player);
    public static event OnTurnStartedHandler OnTurnStarted;

    void Start()
    {
        gameModel.players = new PlayerManager[GameModel.PLAYERS_NUMBER];

        ListenToEvent(Events.PLACE_CARD_ON_FIELD, PlaceCardOnField);
        ListenToEvent(Events.PLACE_CARD_IN_HAND, PlaceCardInHand);
        ListenToEvent(Events.CARD_CASTED_ABILITY, OnCardCastedAbility);
        ListenToEvent(Events.UPDATE_PLAYER_SCORE, UpdatePlayerScore);
        ListenToEvent(Events.END_TURN, EndTurn);
        ListenToEvent(Events.CHECK_END_GAME_CONDITION, CheckEndGameCondition);
        ListenToEvent(Events.GLOW_FIELD_CONTAINERS, GlowFieldContainers);

        GlowFieldContainers(0f, false);
        StartGame();
    }

    void OnDestroy()
    {
        StopListenToEvent(Events.PLACE_CARD_ON_FIELD, PlaceCardOnField);
        StopListenToEvent(Events.PLACE_CARD_IN_HAND, PlaceCardInHand);
        StopListenToEvent(Events.CARD_CASTED_ABILITY, OnCardCastedAbility);
        StopListenToEvent(Events.UPDATE_PLAYER_SCORE, UpdatePlayerScore);
        StopListenToEvent(Events.END_TURN, EndTurn);
        StopListenToEvent(Events.CHECK_END_GAME_CONDITION, CheckEndGameCondition);
        StopListenToEvent(Events.GLOW_FIELD_CONTAINERS, GlowFieldContainers);
    }

    /// <summary>
    /// Starts a new match
    /// </summary>
    void StartGame()
    {
        if (ClientManager.instance.spectating)
        {
            StartCoroutine(SpectateGame());
            return;
        }
        PlayGame();
    }

    /// <summary>
    /// Starts the Player vs AI match
    /// </summary>
    void PlayGame()
    {
        PlayerManager player = Instantiate(gameModel.playerPrefab);
        player.isNearToCamera = true;
        PlayerManager.localPlayer = player;
        BotManager bot = Instantiate(gameModel.botPrefab);
        gameModel.players[0] = player;
        gameModel.players[1] = bot;
        if (OnTurnStarted != null)
        {
            OnTurnStarted(player);
        }
        gameView.HUDView.txtPlayerNames[0].color = Color.yellow;
    }

    /// <summary>
    /// Starts the AI vs AI match
    /// </summary>
    /// <returns></returns>
    IEnumerator SpectateGame()
    {
        for (int i = 0; i < GameModel.PLAYERS_NUMBER; i++)
        {
            gameModel.players[i] = Instantiate(gameModel.botPrefab);
        }
        gameModel.players[0].isNearToCamera = true;
        yield return new WaitForSeconds(1);
        if (OnTurnStarted != null)
        {
            OnTurnStarted(gameModel.players[0]);
        }
        gameView.HUDView.txtPlayerNames[0].color = Color.yellow;
    }

    /// <summary>
    /// Places the chosen card on the field
    /// </summary>
    /// <param name="data"></param>
    void PlaceCardOnField(params object[] data)
    {
        CardManager card = (CardManager)data[0];
        RectTransform cardsContainer = (RectTransform)data[1];
        card.transform.SetParent(cardsContainer, false);
        card.transform.localEulerAngles = Vector3.zero;
        card.TurnCard(true);
        card.ClearDragEvents();
        card.SetDelegates();
        gameModel.currentGrabbedCard = null;
    }

    /// <summary>
    /// Places the chosen card in player's hand
    /// </summary>
    /// <param name="data"></param>
    void PlaceCardInHand(params object[] data)
    {
        CardManager card = (CardManager)data[0];
        card.wasReplacedInHand = true;
        card.transform.SetParent(gameView.HUDView.playerHand, false);
        gameModel.currentGrabbedCard = null;
    }

    /// <summary>
    /// Updates player score UI
    /// </summary>
    /// <param name="data"></param>
    void UpdatePlayerScore(params object[] data)
    {
        int newScore = (int)data[0];
        bool isEnemy = (bool)data[1];

        if (isEnemy)
        {
            gameView.HUDView.txtEnemyScore.text = newScore.ToString();
            return;
        }
        gameView.HUDView.txtPlayerScore.text = newScore.ToString();
    }

    /// <summary>
    /// Ends a player's turn and starts the turn of the other player
    /// </summary>
    /// <param name="data"></param>
    void EndTurn(params object[] data)
    {
        if (gameModel.matchIsOver) { return; }
        PlayerManager lastTurnPlayer = (PlayerManager)data[0];
        for (int i = 0; i < gameModel.players.Length; i++)
        {
            if (gameModel.players[i] == lastTurnPlayer)
            {
                gameView.HUDView.txtPlayerNames[i].color = Color.white;
                continue;
            }
            if (OnTurnStarted != null)
            {
                OnTurnStarted(gameModel.players[i]);
            }
            gameView.HUDView.txtPlayerNames[i].color = Color.yellow;
        }
    }

    /// <summary>
    /// Checks if every player placed all the cards and ends the game
    /// </summary>
    /// <param name="data"></param>
    void CheckEndGameCondition(params object[] data)
    {
        foreach (PlayerManager player in gameModel.players)
        {
            if (player.cardsInHand.Count > 0) { return; }
        }
        gameModel.matchIsOver = true;

        string message;        
        if (gameModel.players[0].playerScore > gameModel.players[1].playerScore)
        {
            message = PlayerManager.localPlayer == null ? "PLAYER 1 WINS" : "VICTORY";
        }
        else if (gameModel.players[0].playerScore == gameModel.players[1].playerScore)
        {
            message = "DRAW";
        }
        else
        {
            message = PlayerManager.localPlayer == null ? "PLAYER 2 WINS" : "DEFEAT";
        }
        GameController.Log("Player 1: " + gameModel.players[0].playerScore + " points");
        GameController.Log("Player 2: " + gameModel.players[1].playerScore + " points");
        GameController.Log(message);
        if (Application.isBatchMode)
        {
            Application.Quit();
            return;
        }
        gameView.HUDView.ShowEndGamePanel(message);
    }

    /// <summary>
    /// Called when a card casted its own ability
    /// </summary>
    void OnCardCastedAbility(params object[] data)
    {
        CardManager cardManager = (CardManager)data[0];
        if (!app.model.cardsReadyToCastAbility.Contains(cardManager)) { return; }
        gameModel.cardsReadyToCastAbility[0].SetCardGlowAlpha(0, 0);
        app.model.cardsReadyToCastAbility.RemoveAt(0);
        cardManager.abilityTargetsNeeded = 0;
        cardManager.abilityTargets.Clear();        
        if (gameModel.cardsReadyToCastAbility.Count < 1) { return; }

        CardManager nextCaster = gameModel.cardsReadyToCastAbility[0];

        if (nextCaster.IsDead && nextCaster.card.ability.castTrigger != Ability.AbilityCastTrigger.death)
        {
            nextCaster.CallOnCardCastedAbility();
            return;
        }
        nextCaster.card.ability.CastAbility(nextCaster.owner, nextCaster, nextCaster.currentCardPosition);
        nextCaster.SetCardGlowAlpha(1, 0.5f);
    }

    /// <summary>
    /// Shows glow effect on the local player's cards containers
    /// </summary>
    /// <param name="data"></param>
    void GlowFieldContainers(params object[] data)
    {
        float alpha = (float)data[0];
        bool fade = (bool)data[1];
        Image imgMeleeCardsContainer = gameView.fieldView.localPlayerMeleeCardsContainer.GetComponent<Image>();
        Image imgRangedCardsContainer = gameView.fieldView.localPlayerRangedCardsContainer.GetComponent<Image>();

        if (fade)
        {
            imgMeleeCardsContainer.CrossFadeAlpha(alpha, 0.5f, false);
            imgRangedCardsContainer.CrossFadeAlpha(alpha, 0.5f, false);
            return;
        }
        imgMeleeCardsContainer.canvasRenderer.SetAlpha(alpha);
        imgRangedCardsContainer.canvasRenderer.SetAlpha(alpha);        
    }

    public static void Log(string message, GameObject target = null)
    {
        if (Application.isBatchMode)
        {
            System.Console.WriteLine(message + System.Environment.NewLine);
            return;
        }
        if (target)
        {
            Debug.Log(message, target);
            return;
        }
        Debug.Log(message);
    }
}
