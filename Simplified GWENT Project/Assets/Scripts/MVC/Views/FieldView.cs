﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class FieldView : View<GameApplication>
{
    //All the field's cards containers
    public RectTransform localPlayerMeleeCardsContainer;
    public RectTransform localPlayerRangedCardsContainer;
    public RectTransform enemyHandContainer;
    public RectTransform enemyMeleeCardsContainer;
    public RectTransform enemyRangedCardsContainer;
}
