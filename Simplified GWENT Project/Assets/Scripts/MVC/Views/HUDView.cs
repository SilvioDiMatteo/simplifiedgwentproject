﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SmartMVC;

public class HUDView : View<GameApplication>
{
    public Transform playerHand;
    public Transform HUDCanvas;
    public Text txtPlayerScore;
    public Text txtEnemyScore;
    public Text[] txtPlayerNames;
    public Text txtGameResult;
    public Button btnBackToMenu;

    void Start()
    {
        btnBackToMenu.onClick.AddListener(OnBackToMenuClick);        
    }

    void OnBackToMenuClick()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        ClientManager.instance.gameObject.SetActive(true);
    }

    public void ShowEndGamePanel(string endGameMessage)
    {
        txtGameResult.transform.parent.gameObject.SetActive(true);
        txtGameResult.text = endGameMessage;
    }
}
