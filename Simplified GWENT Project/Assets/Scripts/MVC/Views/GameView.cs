﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameView : View<GameApplication>
{
    public HUDView HUDView
    {
        get
        {
            if (!myHUDView)
            {
                myHUDView = GetComponentInChildren<HUDView>(true);
            }
            return myHUDView;
        }
    }
    HUDView myHUDView;

    public FieldView fieldView
    {
        get
        {
            if (!myFieldView)
            {
                myFieldView = GetComponentInChildren<FieldView>(true);
            }
            return myFieldView;
        }
    }
    FieldView myFieldView;
}
