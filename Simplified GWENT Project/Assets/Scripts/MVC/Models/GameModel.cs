﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameModel : Model<GameApplication>
{
    /// <summary>
    /// Number of the players in the game
    /// </summary>
    public const byte PLAYERS_NUMBER = 2;

    //Players prefabs
    public PlayerManager playerPrefab;
    public BotManager botPrefab;

    /// <summary>
    /// Players in game
    /// </summary>
    public PlayerManager[] players;

    public GameObject cardPrefab;
    /// <summary>
    /// All the available cards of the game
    /// </summary>
    public Card[] availableCards;
    /// <summary>
    /// The card grabbed by the local player
    /// </summary>
    public CardManager currentGrabbedCard;

    /// <summary>
    /// All the cards that are ready to cast their own ability
    /// </summary>
    public List<CardManager> cardsReadyToCastAbility;

    /// <summary>
    /// Reference to the camera that renders HUD
    /// </summary>
    public Camera HUDCamera;

    /// <summary>
    /// The cursor shown when the player has to target cards
    /// </summary>
    public Texture2D targetCursor;

    /// <summary>
    /// Is the match over?
    /// </summary>
    public bool matchIsOver = false;

    void Start()
    {
        cardsReadyToCastAbility = new List<CardManager>();
    }

    public PlayerManager GetPlayerOpponent(PlayerManager player)
    {
        foreach (PlayerManager currentPlayer in players)
        {
            if (currentPlayer == player) { continue; }
            return currentPlayer;
        }
        return null;
    }

    public int GetPlayerIndex(PlayerManager player)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] != player) { continue; }
            return i;
        }
        return 0;
    }
}
