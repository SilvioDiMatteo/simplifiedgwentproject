﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameApplication : BaseApplication<GameModel, GameView, GameController>
{
    public static GameApplication instance;

    override protected void Awake()
    {
        base.Awake();
        instance = this;
    }
}
