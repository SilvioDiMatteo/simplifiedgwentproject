﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SmartMVC
{
    /// <summary>
    /// Contains all the events of the SmartMVC system
    /// </summary>
    public class Events
    {
        public const string CHECK_END_GAME_CONDITION = "game.checkEndCondition";
        public const string END_TURN = "turn.end";

        public const string PLACE_CARD_IN_HAND = "card.placeInHand";
        public const string PLACE_CARD_ON_FIELD = "card.placeOnField";
        public const string CARD_CASTED_ABILITY = "card.castedAbility";

        public const string UPDATE_PLAYER_SCORE = "player.updateScore";

        public const string GLOW_FIELD_CONTAINERS = "fieldContainers.glow";
    }
}
